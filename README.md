# MoviePreview

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.6.

# What About?
This application show the list of movies and user can see the recent release, and the popular movies.
User can save create there own watch list and can the list of the movies in there watch lists.
Also like the movies and also acn see teh lsit of liked movies.
Fetch al the details about the individual films

#How?
At first user must register with unique username. After the login user can the see the list of movies from which they can create there own watch list.
User can also see the list of the movies without login to the application but to create there watch list and like the movies user must login first

## How Application start
First clone the code with url
then run command `npm install` to install all npm the dependencies.
after that `npm start` or `ng serve` to start the local server
then go to the `http://localhost:4200/` from your browser and you are good to go.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
