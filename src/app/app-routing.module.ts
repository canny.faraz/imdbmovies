import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './pages/common/signin/signin.component';
import { SignupComponent } from './pages/common/signup/signup.component';
import { MoviesComponent } from './pages/private/movies/movies.component';
import { MovieInfoComponent } from './pages/private/movies/movie-info/movie-info.component'

const routes: Routes = [
  {
    path: '',
    component: MoviesComponent
  },
  {
    path: 'signin',
    component: SigninComponent
  },
  {
    path: 'singup',
    component: SignupComponent,
  },
  {
    path: 'movies',
    component: MoviesComponent
  },{
    path: 'movie-details',
    component: MovieInfoComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
