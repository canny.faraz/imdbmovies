import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalstorageService {

  allUsers: any = [];
  isUserLoggedIn: boolean = false;

  constructor() { }

  getUsers(){
    let users = localStorage.getItem('users');
    return this.allUsers = JSON.parse(users) || [];
  }
  
  /**
   * 
   * @param userName check username already registered or not ?
   * check from all the registered users which is stored in localStorage
   * return true if found 
   * else return false
   */
  checkExistingUsers(userName){
    
    let userExist = false;
    let users = this.getUsers();
    
    if(!users.length){
      return userExist;
    }

    users.forEach(user => {
      if(user.user_name == userName){
        userExist = true;
      }
    });

    return userExist
  }

  /**
   * 
   * @param userData takes userdetails from the registration from and
   * pushed into the array of users fetched from local storage i.e. all the existing users
   * then store into the local storage
   */

  registerUser(userData){
    this.allUsers.push(userData);
    localStorage.setItem('users', JSON.stringify(this.allUsers));
  }

  /**
   * 
   * @param userName check username exists or not
   */
  getUser(userName){
    let allUsers = this.getUsers();
    let userData;
    allUsers.forEach(user => {
      if(user.user_name == userName){
        userData = user;
      }
    });
    return userData
  }

  /**
   * 
   * @param movie take movie details as an argument and store into the array of the movies
   * based on which user is loggedIn at that time with username as key of watchList Object
   */

  addToWatchList(movie){
    let loggedInUser = JSON.parse(localStorage.getItem('loggedInUser'));
    let watchList = JSON.parse(localStorage.getItem('watchList')) || {};
    if(watchList && !watchList[loggedInUser.user_name]){
      watchList[loggedInUser.user_name] = [movie]
    }else if(watchList && watchList[loggedInUser.user_name] && watchList[loggedInUser.user_name].length){
      watchList[loggedInUser.user_name].push(movie);
    }
    localStorage.setItem('watchList', JSON.stringify(watchList))
  }

  /**
   * 
   * @param movie take movie details as an argument and store into the array of the movies
   * based on which user is loggedIn at that time with username as key of likedMovies Object
   */
  addLikedMovieList(movie){
    let loggedInUser = JSON.parse(localStorage.getItem('loggedInUser'));
    let watchList = JSON.parse(localStorage.getItem('likedMovies')) || {};
    if(watchList && !watchList[loggedInUser.user_name]){
      watchList[loggedInUser.user_name] = [movie]
    }else if(watchList && watchList[loggedInUser.user_name] && watchList[loggedInUser.user_name].length){
      watchList[loggedInUser.user_name].push(movie);
    }
    localStorage.setItem('likedMovies', JSON.stringify(watchList))
  }

  /**
   * fetch all the watch list of the loggedIn user from localStorage
   * if not then return the empty array
   */
  getWatchList(){
    let loggedInUser = JSON.parse(localStorage.getItem('loggedInUser'));
    let watchList = JSON.parse(localStorage.getItem('watchList'));
    return watchList && watchList[loggedInUser.user_name] || []
  }

  /**
   * fetch all the liked movies of the loggedIn user from localStorage
   * if not then return the empty array
   */
  getLikedMovies(){
    let loggedInUser = JSON.parse(localStorage.getItem('loggedInUser'));
    let likedMovies = JSON.parse(localStorage.getItem('likedMovies'));
    return likedMovies && likedMovies[loggedInUser.user_name] || []
  }
}
