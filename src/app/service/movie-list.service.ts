import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MovieListService {

  /**
   * Buffer to store the movies details initialise as NULL
   */
  private movieDataSource = new BehaviorSubject<any>(null);

  /**
   * Movie details subscriber for further use 
   */
  movieDetails$ = this.movieDataSource.asObservable();

  constructor(private http: HttpClient) { }
  poster_base_url = "https://image.tmdb.org/t/p/w500/";

  /**
   * 
   * @param movie takes movie details (Object) as an argument and update into the Buffer (BS i.e BehaviourSubject)
   * for further use in our application
   */
  updatemovieDetails(movie) {
    this.movieDataSource.next(movie);
  }

  /**
   * 
   * @param url common execute function for http call takes URL as an argument in String
   */
  execute( url: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': "application/json"
      }),
   
    };
    return this.http.get<any>(url, httpOptions);
  }
  /**
   * 
   * @param page takes page number in number as argument and return the POPULAR MOVIES (array of movies) page wise
   */
  getFamousMovies(page){
    return this.execute("https://api.themoviedb.org/3/movie/popular?api_key=4b10cf2f8e6ed1fcb506bd3929ecee40&language=en-US&page=" + page)
  }

  /**
   * 
   * @param page takes page number in number as argument and return the RECENT RELASED MOVIES (array of movies) page wise
   */
  getRecentMovies(page){
    return this.execute('https://api.themoviedb.org/3/movie/upcoming?api_key=4b10cf2f8e6ed1fcb506bd3929ecee40&page=' + page)
  }
}
