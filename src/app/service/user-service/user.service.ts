import { Injectable } from '@angular/core';
import { LocalstorageService } from '../localstorage/localstorage.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  /**
   * LoggedIn user data source for validating user loogedin or not? in the application
   */
  private loggedIn = new BehaviorSubject<any>(null);

  /**
   * observable which can be subscribe in our application
   */
  userLoggedIn$ = this.loggedIn.asObservable();

  /**
   * 
   * @param userDetails is the details of the loggedIn user whhich updated in Subject so that
   * we can access anywhere we want
   */
  updateLoggedIn(userDetails) {
    this.loggedIn.next(userDetails);
  }

  constructor(private _ls: LocalstorageService) {
    /**
     * checking if user loggedIn or not?
     * If yes then update the same.
     */
    let userLoggedIn = this.isLogin()
    if(userLoggedIn){
      this.updateLoggedIn(userLoggedIn)
    }
  }
  /**
   * 
   * @param userPayload userdetails to save into the localStorage and 
   * also update the same details to our BehaviourSubject for further use
   */
  login(userPayload){
    localStorage.setItem('loggedInUser', JSON.stringify(userPayload));
    this.updateLoggedIn(userPayload);
  }

  /**
   * this function remove the loggedInUser details from localStorage and
   * update NULL in BehaviourSubject so that we can update the same in all other places 
   */
  logout(){
    localStorage.removeItem('loggedInUser');
    this.updateLoggedIn(null)
  }
  /**
   * 
   * @param userName Takes username as argument and check if this user already registered or not?
   * if exists then return true
   * else return false
   */
  checkUserExists(userName){
    let userDetails = this._ls.getUser(userName);
    if(userDetails){
      return true;
    }else{
      return false;
    }
  }

  /**
   * 
   * @param userPayload takes all the payload 
   * {
   *  user_name: <user_name>
   *  password: <password>
   * }
   * 
   * then check from the local storage for correct password
   */
  checkPassword(userPayload){
    let userDetails = this._ls.getUser(userPayload.user_name);
    if(userDetails.password == userPayload.password){
      return true;
    }else{
      return false;
    }
  }

  /**
   * fetch loggedIn user details from the localStorage and return the same
   */
  isLogin(){
    let loggedInUser = JSON.parse(localStorage.getItem('loggedInUser'));
    return loggedInUser;
  }
}
