import { AbstractControl } from '@angular/forms';

export function SamePass(compareTo: any) {
	return function(control: AbstractControl) {        
		if (compareTo && compareTo == control.value) {
			return null;
		}
		return { SamePass: true };
	};
}