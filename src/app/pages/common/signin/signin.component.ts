import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/service/user-service/user.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  /**
   * TO populate the reactive form
   */
  siginForm: FormGroup;

  constructor(
    private _fb: FormBuilder,
    private _us: UserService,
    private _router: Router
  ) {
    this.siginForm = _fb.group({
      user_name: [null, Validators.required],
      password: [null, Validators.required]
    });
  }

  ngOnInit() {}

  /**
   * 
   * @param userName takes username as argument to check whether filled username
   * is valid or not before the login.
   * if not showing alert that No user exist with provided username and make the field required
   */
  checkUser(userName){
    let self = this;
    if(!self._us.checkUserExists(userName)){
      Swal.fire({
        title: 'Error!',
        text: 'No such user exists',
        icon: 'error',
        confirmButtonText: 'Okay',
        timer: 3000
      }).then(() => {
        self.siginForm.controls.user_name.reset();
        self.siginForm.controls.user_name.markAsDirty();
        self.siginForm.controls.user_name.markAsTouched();
      })
    }
  }

  /**
   * 
   * @param payload userDetails as an argument
   * if user pass all the validation like
   *  1. All required field is correctly filled
   *  2. Password Check?
   *    2.a If worng pasword then show alert
   *    2.b Password Correct => Show success alert and store the loggedIn userdetails in localStorage
   */
  login(payload){
    if(!this.isFormValid()){
      this.markForm();
      return
    }
    if(this._us.checkPassword(payload)){
      this._us.login(payload);
      Swal.fire({
        title: 'Success!',
        text: 'Login Successfful',
        icon: 'success',
        confirmButtonText: 'Okay'
      }).then(()=>{
        this._router.navigate(['movies'])
      });
    }else{
      Swal.fire({
        title: 'Error!',
        text: 'Incorrect Password!!',
        icon: 'error',
        confirmButtonText: 'Okay'
      });
    }
  }

  /**
   * form cvalidation check
   */
  isFormValid(){
    return this.siginForm.valid
  }

  /**
   * if field is not valid the check for the validation to show the correct error message of the corresponding field
   */
  markForm(){
    let inputAryVar = this.siginForm.controls
    for(let keyVar in inputAryVar){
      if(!inputAryVar[keyVar].valid){
        inputAryVar[keyVar].markAsTouched();
        inputAryVar[keyVar].markAsDirty();
      }
    }
  }

}
