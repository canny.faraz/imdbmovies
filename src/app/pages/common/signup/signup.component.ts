import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LocalstorageService } from '../../../service/localstorage/localstorage.service';
import Swal from 'sweetalert2';
import { SamePass } from '../../../validator/SamePass.validators'
import { UserService } from 'src/app/service/user-service/user.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signupForm: FormGroup;

  constructor(
    private _fb: FormBuilder,
    private _ls: LocalstorageService,
    private _us: UserService
  ) {
    this.signupForm = this._fb.group({
        user_name: [null, Validators.required],
        password: [null, Validators.required],
        conf_password: [null, [Validators.required]],
        email: [null, [Validators.required, Validators.email]]
      }
    );
  }


  ngOnInit() {}

  /**
   * 
   * @param userName check userName filled by user is already takes or not?
   */
  checkUser(userName){
    let self = this;
    if(self._us.checkUserExists(userName)){
      Swal.fire({
        title: 'Error!',
        text: 'User already exists',
        icon: 'error',
        confirmButtonText: 'Okay',
        timer: 1000
      }).then(() => {
        self.signupForm.controls.user_name.reset();
        self.signupForm.controls.user_name.markAsDirty();
        self.signupForm.controls.user_name.markAsTouched();
      })
    }
  }

  /**
   * if user pass all the validation like
   *  1. All required field is correctly filled
   *  2. Check userName Exist?
   *    If exists then show alert and update validation
   *    else save the userDetails 
   */
  register(){
    let self = this;
    let userData = self.signupForm.value;

    if(self.signupForm.valid){
      let userExists = self._ls.checkExistingUsers(userData.user_name)
      
      if(!userExists){
        self._ls.registerUser(userData);
        Swal.fire({
          icon: 'success',
          title: 'Register Successfully!!',
          timer: 3000,
          confirmButtonText: 'Okay'
        }).then(()=>{
          self.signupForm.reset();
        });
      }else{
        Swal.fire({
          title: 'Error!',
          text: 'User already exist please choose different user name',
          icon: 'error',
          confirmButtonText: 'Okay'
        }).then(() => {
          self.signupForm.controls.user_name.reset();
          self.signupForm.controls.user_name.markAsTouched();
          self.signupForm.controls.user_name.markAsDirty();
        })
      }
    }else if(!self.signupForm.valid){
      self.setTouchedAndDirty();
      console.log("self.signupForm.valid", self.signupForm);
      
    }
  }

  /**
   * if field is not valid the check for the validation to show the correct error message of the corresponding field
   */
  setTouchedAndDirty(){
    let inputAryVar = this.signupForm.controls
    for(let keyVar in inputAryVar){
      if(!inputAryVar[keyVar].valid){
        inputAryVar[keyVar].markAsTouched();
        inputAryVar[keyVar].markAsDirty();
      }
    }
  }

  /**
   * change the validation to check whether filled comfirm password is same as password or not
   */
  checkPassword(){
    this.signupForm.get('conf_password').setValidators([Validators.required, SamePass(this.signupForm.get('password').value)]);
    this.signupForm.get('conf_password').updateValueAndValidity();
  }

}
