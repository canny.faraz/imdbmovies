import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { UserService } from 'src/app/service/user-service/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  /**
   * for validating loggedIn user
   */
  isUserLoggedIn: boolean = false;
  /**
   * for displaying user details like user name if loggedin
   */
  loggedInuser: any;

  /**
   * for updating active class
   */
  activeClass: any = {
    movies: false,
    signin: false,
    singup: false
  }

  constructor(private _router: Router, private _us: UserService) { 
    let self = this;

    /**
     * subcribed to check whether user loggedIn or not?
     */
    this._us.userLoggedIn$.subscribe(loggedInuser =>{      
      if(loggedInuser){
        self.isUserLoggedIn = true;
        self.loggedInuser = loggedInuser
      }else{
        self.isUserLoggedIn = false;
        self.loggedInuser = null
      }
    })
  }

  ngOnInit() {
    if (this._router.url == '/signin') {
      this.activateTag('sigin');
    }
    this._router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        //calls self stuff when navigation ends          
        this.activateTag(event.url.split('/')[1])
      }
    });
  }

  /**
   * 
   * @param activeTag takes tag as an input to manuaaly update the active class
   */
  activateTag(activeTag) {
    let self = this;
    Object.keys(this.activeClass).forEach((tag) => {
      if (activeTag == tag && self._router.url.split('/').length == 2) {
        self.activeClass[tag] = true;
      }
      else {
        self.activeClass[tag] = false;
      }
    })
  }

  /**
   * to signout
   */
  logout(){
    this._us.logout();
    this._router.navigate(['signin'])
  }
}
