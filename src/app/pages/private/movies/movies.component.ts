import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { MovieListService } from '../../../service/movie-list.service'
import { UserService } from 'src/app/service/user-service/user.service';
import { Router } from '@angular/router';
import { LocalstorageService } from 'src/app/service/localstorage/localstorage.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  recentMovies: any;
  showRecentMovies: boolean;
  showPopularMovies: boolean;
  isUserLogin: any;
  showWatchList: boolean;
  showLikedMoview: boolean;

  constructor(
    public movie_service: MovieListService, 
    private _us: UserService, 
    private _router: Router,
    private _ls: LocalstorageService
  ) {
    this._us.userLoggedIn$.subscribe(data=>{
      if(data){
        this.isUserLogin = true
      }else{
        this.isUserLogin = false
      }
    })
  }
  poster_url: any[]
  movies_content
  imdb_ratings = [];
  movies_title: any[];
  currentPage: number = 1;
  // pages: Array<number>;
  showChart: boolean = true;
  showMovies: boolean = false;
  pageToshow:number = 1;
  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,

  };

  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    {
      data: this.imdb_ratings, label: 'Rating', backgroundColor: 'rgba(79, 255, 195, 1)', hoverBackgroundColor: 'rgba(79, 255, 195, 1)'
    },
  ];
  ngOnInit() {
    this.getPopularMovies();
  }
  
  /**
   * 
   * @param pageNumber get Popular movie details according to the page number
   */
  getPopularMovies(pageNumber: number = 1) {
    this.movie_service.getFamousMovies(pageNumber).subscribe(data => {      
      this.movies_content = data;
      this.showPopularMovies = true;
      this.showRecentMovies = false;
      this.showWatchList = false
      this.showLikedMoview = false
      this.poster_url = this.movies_content.results.map(content => content.poster_path);
      this.movies_title = this.movies_content.results.map(content => content.title);
      this.imdb_ratings = this.movies_content.results.map(content => content.vote_average);
      this.barChartData[0].data = this.imdb_ratings;
    })
  }

  /**
   * 
   * @param pageNumber get getTopRatedMovies details according to the page number
   */
  getTopRatedMovies(pageNumber){
    this.movie_service.getRecentMovies(pageNumber).subscribe(data => {
      console.log("Top rated movies", data);
      this.movies_content = data;
      this.showPopularMovies = false;
      this.showRecentMovies = true;
      this.showChart = false;
      this.showMovies = true;
      this.showWatchList = false
      this.showLikedMoview = false
    })
  }
  
  /**
   * 
   * @param pages previous page 
   * call to fetch movie details based on which tpe of movies is displayed currently
   */
  goPrevious(pages) {
    console.log("page no", pages);
    if(this.showPopularMovies){
      this.getPopularMovies(pages)
    }else if(this.showRecentMovies){
      this.getTopRatedMovies(pages);
    }
  }
  
  /**
   * 
   * @param pages Next page 
   * call to fetch movie details based on which tpe of movies is displayed currently
   */
  goNext(pages) {
    console.log("page no", pages);
    if(this.showPopularMovies){
      this.getPopularMovies(pages)
    }else if(this.showRecentMovies){
      this.getTopRatedMovies(pages);
    }
  }
  
  /**
   * set boolean to diplayy the trending charts
   */
  displayChart() {
    this.showMovies = false
    this.showChart = true;
  }
  
  /**
   * update the boolean to diplaying list of movies
   */
  displayPoster() {
    this.getPopularMovies();
    this.showChart = false;
    this.showMovies = true;
  }

  /**
   * @param movie Argument Movie details
   * First, check user loggedIn or not?
   * if LoggedIn => add movie to watch list
   * else => ask user to login fist and redirect to login page
   */
  addToWatchList(movie){
    let self = this;
    if(this.isUserLogin){
      this._ls.addToWatchList(movie);
      Swal.fire({
        title: 'Success!!',
        text: 'Movie Added to your watch list successfully',
        icon: 'success',
        confirmButtonText: 'Okay'
      })
    }else{
      Swal.fire({
        title: 'Error!!',
        text: 'Please login first to add the movie to ypur watch list.',
        icon: 'error',
        confirmButtonText: 'Okay'
      })
    }
  }

  /**
   * @param movie Argument Movie details
   * First, check user loggedIn or not?
   * if LoggedIn => add movie to Liked movies
   * else => ask user to login fist and redirect to login page
   */
  addToLikesList(movie){
    let self = this;
    if(this.isUserLogin){
      this._ls.addLikedMovieList(movie);
      Swal.fire({
        title: 'Success!!',
        // text: 'Movie Added to your watch list successfully',
        icon: 'success',
        confirmButtonText: 'Okay'
      })
    }else{
      Swal.fire({
        title: 'Error!!',
        text: 'To like this movie you have to login first.',
        icon: 'error',
        confirmButtonText: 'Okay'
      }).then(()=>{
        self._router.navigate(['signin'])
      })
    }
  }

  /**
   * 
   * @param movie update the movie to buffer and redirect to movie details view page
   */
  showMovieDetails(movie){
    this.movie_service.updatemovieDetails(movie)
    this._router.navigate(['movie-details'], { queryParams: {details:movie.title } })
  }

  /**
   * fetch all the watch list of the loggedIn user and update the boolean to display the same
   */
  getWatchList(){
    console.log(this._ls.getWatchList());
    this.movies_content['results'] = this._ls.getWatchList();
    this.showPopularMovies = false;
    this.showRecentMovies = false;
    this.showChart = false;
    this.showMovies = true;
    this.showWatchList = true
    this.showLikedMoview = false
  }

  /**
   * fetch all the liked movies of the loggedIn and update the boolean to display the same
   */
  getLikedMovies(){
    console.log(this._ls.getLikedMovies());
    this.movies_content['results'] = this._ls.getLikedMovies();
    this.showPopularMovies = false;
    this.showRecentMovies = false;
    this.showChart = false;
    this.showMovies = true;
    this.showLikedMoview = true;
    this.showWatchList = false
  }

}
