import { Component, OnInit, OnDestroy } from '@angular/core';
import { MovieListService } from 'src/app/service/movie-list.service';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/service/user-service/user.service';
import { LocalstorageService } from 'src/app/service/localstorage/localstorage.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-movie-info',
  templateUrl: './movie-info.component.html',
  styleUrls: ['./movie-info.component.css']
})
export class MovieInfoComponent implements OnInit, OnDestroy {

  movieSubscriber: Subscription
  showDetails: boolean;
  movie: any;
  isUserLogin: boolean;

  constructor(private _movie_service: MovieListService, private _us: UserService,private _ls: LocalstorageService,private _router: Router) {
    this.movieSubscriber = this._movie_service.movieDetails$.subscribe(data => {
      if(data){
        this.showDetails = true;
        this.movie = data;
      }      
    });

    this._us.userLoggedIn$.subscribe(data=>{
      if(data){
        this.isUserLogin = true
      }else{
        this.isUserLogin = false
      }
    })
  }

  ngOnInit() {
  }

  /**
   * unsubcribe the subscription for preventing the data leakage
   */
  ngOnDestroy(){
    this.movieSubscriber.unsubscribe();
  }

  /**
   * @param movie Argument Movie details
   * First, check user loggedIn or not?
   * if LoggedIn => add movie to watch list
   * else => ask user to login fist and redirect to login page
   */
  addToWatchList(movie){
    let self = this;
    if(this.isUserLogin){
      this._ls.addToWatchList(movie);
      Swal.fire({
        title: 'Success!!',
        text: 'Movie Added to your watch list successfully',
        icon: 'success',
        confirmButtonText: 'Okay'
      })
    }else{
      Swal.fire({
        title: 'Error!!',
        text: 'Please login first to add the movie to ypur watch list.',
        icon: 'error',
        confirmButtonText: 'Okay'
      }).then(()=>{
        self._router.navigate(['signin'])
      })
    }
  }

  /**
   * @param movie Argument Movie details
   * First, check user loggedIn or not?
   * if LoggedIn => add movie to Liked movies
   * else => ask user to login fist and redirect to login page
   */
  addToLikesList(movie){
    let self = this;
    if(this.isUserLogin){
      this._ls.addLikedMovieList(movie);
      Swal.fire({
        title: 'Success!!',
        // text: 'Movie Added to your watch list successfully',
        icon: 'success',
        confirmButtonText: 'Okay'
      })
    }else{
      Swal.fire({
        title: 'Error!!',
        text: 'To like this movie you have to login first.',
        icon: 'error',
        confirmButtonText: 'Okay'
      }).then(()=>{
        self._router.navigate(['signin'])
      })
    }
  }

}
